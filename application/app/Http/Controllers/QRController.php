<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Mpdf\Mpdf;
use Carbon\Carbon;

class QRController extends Controller
{
    public function makePDF(Request $request)
    {
        $uid = rand(111111, 99999);
        $data = "https://dutabash.com/document_verify?uid=$uid";
        $to = ('assets/qrcodes/');
        $filename = $to . $uid . '.png';
        $cert = $to . $uid . '.pdf';
        file_put_contents($cert, public_path('assets/qrcodes/dummy-pdf.pdf'));
        QrCode::encoding('UTF-8')->format('png')->size(275)->errorCorrection('H')->generate($data, $filename);


        $pdf = new \Mpdf\Mpdf();
        $pdf->SetImportUse();
        $pagecount = $pdf->SetSourceFile(('assets/dummy-pdf.pdf'));
        for ($i = 1; $i <= $pagecount; $i++) {

            $import_page = $pdf->ImportPage($i);
            $pdf->UseTemplate($import_page);

            $pdf->WriteFixedPosHTML('<div style="background-color: #ffffff; width: 175px; font-size: 8px; text-align: center; padding: 5px">
                                                    <span style="font-family: nikosh; font-size: 10px; margin: 0 0 -3px;">Ministry of Foreign Affairs</span>
                                                    <div>
                                                        <img src="' . $filename . '" alt="">
                                                    </div>
                                                    <strong>For verification please visit: </strong> www.mofa.gov.bd <br>
                                                    <strong>MOFA UID: </strong> ' . $uid . '<br/><strong>Valid till:</strong> ' . date("d-m-Y", strtotime(date('Y-m-d', strtotime(Carbon::today()->addDays(180))))) . '
                                                </div>', 170, 235, 30, 40.1);
            $pdf->Image('assets/bd_logo.png', 181, 252, 8, 8, 'svg', '', true, false);


            if ($i < $pagecount) {
                $pdf->AddPage();
            }
        }

        return $pdf->Output();
    }
}
